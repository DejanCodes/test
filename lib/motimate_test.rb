require 'bundler/setup'

require 'csv'
require 'faraday'
require 'pry'

require_relative 'motimate_test/parser'
require_relative 'motimate_test/example_api_client'
require_relative 'motimate_test/backups_remover'
require_relative 'motimate_test/motimate_test_script'

