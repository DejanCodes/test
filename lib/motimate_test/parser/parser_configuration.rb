class ParserConfiguration
  attr_reader :columns
  Column = Struct.new(:name, :col_number, :type)

  def initialize
    @columns = []
  end

  def string(name, column: )
    @columns << Column.new(name, column, -> (x) { x.to_s })
  end

  def datetime(name, column: )
    @columns << Column.new(name, column, -> (x) { DateTime.parse(x) })
  end
end
