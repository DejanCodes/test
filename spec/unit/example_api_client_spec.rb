require_relative '../../lib/motimate_test/example_api_client'

RSpec.describe ExampleApiClient do
  subject(:example_api_client) { described_class.new }

  describe ".new" do
    it "has a proper root_url" do
      expect(example_api_client.root_url).to eq("https://example.com")
    end
  end

  describe "#remove_backup!" do
    subject { example_api_client.remove_backup!(id: id) }
    let(:id) { 1 }
    let(:path) { "remove-backup?backup_id=#{id}" }
    let(:connection) { instance_double(Faraday::Connection) }
    let(:response) { instance_double(Faraday::Response) }

    before do
      allow(Faraday).to receive(:new).and_return(connection)
    end

    it "makes a PUT request to remove-backup url and returns a response" do
      expect(connection).to receive(:put).with(path, {}).and_return(response)
      expect(subject).to eq(response)
    end
  end
end