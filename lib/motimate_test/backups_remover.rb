require_relative 'backups_remover/results_logger'

class BackupsRemover
  def initialize(api_client: nil, results_logger: nil, results_log_file_path: nil)
    @api_client = api_client || ExampleApiClient.new
    @results_logger = results_logger || ResultsLogger
    @results_log_file_path = results_log_file_path || "backups_remover_log.txt"
  end

  def remove!(ids:)
    results = []

    ids.each do |id|
      print "."
      response = @api_client.remove_backup!(id: id)
      results << result_data_object(id: id, response: response)
    end

    log_request_results(results)
  end

  private

  def result_data_object(id:, response:)
    {
      id: id,
      code: response.status,
      body: response.body
    }
  end

  def log_request_results(results)
    @results_logger.new(results).write_to_file(@results_log_file_path)
    puts "Removal finished!"
  end
end