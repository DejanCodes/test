class ResultsLogger
  attr_reader :results

  def initialize(results)
    @results = results
  end

  def write_to_file(path)
    timestamp = Time.now.utc.strftime "%Y-%m-%d %H:%M:%S %z"

    File.open(path, "a+") do |file|
      results.each do |res|
        file.puts "#{res[:id]}\t#{timestamp}" + result_details(res) + "\n\n"
      end
    end
  end

  private

  def result_details(result)
    if result[:code] == 200
      "\tRemoved\t200"
    else
      "\tError\t#{result[:code]}\t#{result[:body]}"
    end
  end
end