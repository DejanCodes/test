# require_relative "../../lib/motimate_test/parser"
#
# RSpec.describe Parser do
#   describe "." do
#     subject { described_class.new(configuration: parser_configuration) }
#     let(:parser_configuration) { instance_double(ParserConfiguration) }
#
#     it "creates an instance with proper configuration" do
#       expect(subject.instance_variable_get(:@configuration)).to eq(parser_configuration)
#     end
#   end
#
#   describe "#from_file" do
#     subject  do
#       described_class.new(configuration: parser_configuration).from_file(file_path) do |config|
#         config.string :id, column: 1
#         config.datetime :created_at, column: 2
#       end
#     end
#     let(:parser_configuration) { instance_double(ParserConfiguration, columns: []) }
#     let(:columns) { [] }
#
#     before do
#       allow(ParserConfiguration).to receive(:new).and_return(parser_configuration)
#     end
#
#     context "with valid file" do
#       let(:file_path) { "spec/support/fixtures/test_file.txt" }
#
#       it "calls proper configuration" do
#         expect(parser_configuration).to receive(:string)
#         expect(parser_configuration).to receive(:datetime)
#         expect(parser_configuration).to receive(:columns)
#
#         subject
#       end
#     end
#   end
# end