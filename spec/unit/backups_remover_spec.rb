require_relative '../../lib/motimate_test/backups_remover'

RSpec.describe BackupsRemover do
  describe "#remove!" do
    subject { described_class.new(results_log_file_path: results_log_file_path).remove!(ids: [id]) }
    let(:api_client) { instance_double(ExampleApiClient) }
    let(:response) { instance_double(Faraday::Response, status: 200, body: {some: "body"}) }
    let(:results_logger) { instance_double(ResultsLogger) }
    let(:results_log_file_path) { "some/path.txt" }
    let(:id) { 1 }
    let(:results) do
      [
        {
          id: id,
          code: response.status,
          body: response.body
        }
      ]
    end

    before do
      allow(ExampleApiClient).to receive(:new).and_return(api_client)
      allow(api_client).to receive(:remove_backup!).with(id: id).and_return(response)
    end

    it "sends the results object to ResultsLogger" do
      expect(ResultsLogger).to receive(:new).with(results).and_return(results_logger)
      expect(results_logger).to receive(:write_to_file).with(results_log_file_path)
      subject
    end
  end
end