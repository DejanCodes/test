class ExampleApiClient
  attr_reader :root_url

  def initialize(root_url: "https://example.com")
    @root_url = root_url
  end

  def remove_backup!(id:, params: {})
    path = "remove-backup?backup_id=#{id}"
    response = connection.put(path, params)

    response
  end

  private

  def connection
    @connection ||= Faraday.new(url: @root_url) do |faraday|
      faraday.request :url_encoded
      faraday.adapter Faraday.default_adapter
    end
  end
end