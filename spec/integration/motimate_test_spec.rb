require_relative '../../lib/motimate_test'
require 'tempfile'

RSpec.describe MotimateTestScript do
  subject { described_class.new(backups_list_path: arguments.first, results_log_file_path: arguments.last).run }
  let(:arguments) { [test_in_file.path, test_out_file.path] }
  let(:test_out_file) do
    Tempfile.new('csv').tap do |f|
      f << out_file_headers.join("\t") + "\n\n"
      f.close
    end
  end
  let(:out_file_headers) { ['ID', 'Created at', 'Status', 'Code', 'ErrorDescription'] }

  let(:test_in_file) do
    Tempfile.new('csv').tap do |f|
      input_rows.each do |row|
        f << row.join("  ") + "\n\n"
      end

      f.close
    end
  end

  let(:input_rows) do
    # This is a bit too many examples than necessary, but I wanted to reuse exactly the same list as in the task description.
    [
      ['ID', 'Created at', 'Status Size', 'Database'],
      ['a810', '2019-07-10 00:08:02 +0000', 'Completed 2019-07-10 00:18:53 +0000', '482.43MB', 'DATABASE'],
      ['a179', '2020-07-09 00:06:19 +0000', 'Completed 2019-07-09 00:23:42 +0000', '480.99MB', 'DATABASE'],
      ['a278', '2019-07-08 00:06:19 +0000', 'Completed 2019-07-08 00:16:04 +0000', '479.59MB', 'DATABASE'],
      ['a827', '2019-07-07 00:08:02 +0000', 'Completed 2019-07-07 00:18:43 +0000', '481.75MB', 'DATABASE']
    ]
  end

  before do
    Timecop.freeze(Time.parse("2021-02-22 00:06:19 +0000"))
    uri_template = Addressable::Template.new "https://example.com/remove-backup?backup_id={id}"
    stub_request(:put, uri_template).to_return(status: 200)
  end

  after do
    Timecop.return
    test_in_file.unlink
    test_out_file.unlink
  end

  shared_examples "every request result has been logged" do
    it "adds all request results" do
      subject
      expect(CSV.open(test_out_file.path).readlines).to eq(expected_output_rows)
    end
  end

  context "when all removals succeeded" do
    let(:expected_output_rows) do
      [
        ["ID\tCreated at\tStatus\tCode\tErrorDescription"],
        [],
        ["a810\t2021-02-22 00:06:19 +0000\tRemoved\t200"],
        [],
        ["a179\t2021-02-22 00:06:19 +0000\tRemoved\t200"],
        [],
        ["a278\t2021-02-22 00:06:19 +0000\tRemoved\t200"],
        [],
        ["a827\t2021-02-22 00:06:19 +0000\tRemoved\t200"],
        []
      ]
    end

    it_behaves_like "every request result has been logged"
  end

  context "when some of the removal requests failed" do
    let(:expected_output_rows) do
      [
        ["ID\tCreated at\tStatus\tCode\tErrorDescription"],
        [],
        ["a810\t2021-02-22 00:06:19 +0000\tRemoved\t200"],
        [],
        ["a179\t2021-02-22 00:06:19 +0000\tError\t500\t{error: 'invalid ID'}"],
        [],
        ["a278\t2021-02-22 00:06:19 +0000\tError\t404\t{error: 'Not Found'}"],
        [],
        ["a827\t2021-02-22 00:06:19 +0000\tRemoved\t200"],
        []
      ]
    end

    before do
      stub_request(:put, "https://example.com/remove-backup?backup_id=a179").to_return(status: 500, body: "{error: 'invalid ID'}")
      stub_request(:put, "https://example.com/remove-backup?backup_id=a278").to_return(status: 404, body: "{error: 'Not Found'}")
    end

    it_behaves_like "every request result has been logged"
  end

  context "when some of the backups are not old enough" do
    let(:fresh_backups) { %w[a123 b456] }

    before do
      input_rows << [fresh_backups.first, '2021-02-20 00:00:00 +0000', 'Completed 2021-02-20 00:00:00 +0000', '482.43MB', 'DATABASE']
      input_rows << [fresh_backups.last, '2021-02-21 00:00:00 +0000', 'Completed 2021-02-21 00:00:00 +0000', '482.43MB', 'DATABASE']
    end

    it "does not send a request to remove them" do
      subject
      expect(a_request(:put, /example.com\/remove-backup/)).to have_been_made.times(4)
      fresh_backups.each do |id|
        expect(a_request(:put, "https://example.com/remove-backup?backup_id=#{id}")).not_to have_been_made
      end
    end
  end
end
