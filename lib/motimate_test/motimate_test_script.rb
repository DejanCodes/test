class MotimateTestScript
  OLDER_THAN_IN_DAYS = 30

  def initialize(backups_list_path:, parser: nil, backups_remover: nil, results_log_file_path: nil)
    @backups_list_path = backups_list_path
    @parser = parser || Parser.new
    @backups_remover = backups_remover || BackupsRemover.new(results_log_file_path: results_log_file_path)
  end

  def run
    backups = parse_backups_list_file
    ids = find_old_backups(backups: backups, older_than_in_days: OLDER_THAN_IN_DAYS)
    remove_old_backups(ids: ids)
  end

  private

  def parse_backups_list_file
    parsed_records = @parser.from_file(@backups_list_path) do |config|
      config.string :id, column: 1
      config.datetime :created_at, column: 2
    end

    parsed_records
  end

  def find_old_backups(backups:, older_than_in_days:)
    border_date = Date.today - older_than_in_days # możę jeszcze .to_datetime ???
    last_backups = backups.select { |r| r[:created_at] < border_date }
    ids = last_backups.map { |h| h[:id] }

    ids
  end

  def remove_old_backups(ids:)
    @backups_remover.remove!(ids: ids)
  end
end