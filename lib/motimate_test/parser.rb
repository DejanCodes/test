require_relative './parser/parser_configuration'

class Parser
  attr_reader :configuration

  def initialize(configuration: ParserConfiguration.new)
    @configuration = configuration
  end

  def from_file(path, **options)
    yield configuration
    rows = CSV.read( path, { col_sep: "  ",
                             row_sep: "\n\n",
                             headers: true }.merge(options) )

    process(rows)
  end

  private

  def process(rows)
    rows.map { |row| process_row(row) }
  end

  def process_row(row)
    obj = {}
    @configuration.columns.each do |col|
      obj[col.name] = col.type.call(row[col.col_number - 1])
    end
    obj
  end
end

